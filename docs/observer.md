<div align="center">
  <h1>Observer</h1>
</div>

<div align="center">
  <img src="observer_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **A behavioral pattern to notify multiple objects about changes to the subject they subscribed to.**



### Real-World Analogy

_A newspaper subscription._

The newspaper publisher (analogy for Subject) provide news to its subscribers (Observers).

### Participants

- :bust_in_silhouette: **Publisher**
  - Also referred to as **Subject** or **Event Manager**.
  - Defines an interface to:
      - `subscribe` subscribers (aka `attach`)
      - `unsubscribe` subscribers (aka `detach`)
      - `setStateA`: set the publishers state
  - Optionally provides an interface to:
      - `notify` subscribers
      - `getStateA`: get publisher state

- :bust_in_silhouette: **Subscriber** 
  - Also known as **Observer** or **Event Listener**.
  - Defines an interface to:
      - `update` with a _notification_ (aka _message_ or _event_) from the publisher.


- :man: **ConcretePublisher**
  - Stores state of interest to ConcreteSubscriber objects
  - Provides implementation for:
      - `subscribe` 
      - `unsubscribe` 
      - `setStateA`: sends notifications to its subscribers when its state changes.

- :man: **ConcreteSubscriber**
  - Provides implementation for:
    - `update`   

### Collaborations

**Subscribers** are _connected_ (aka _subscribed_ or _attached_) to the **Publisher**

**Publisher** notifies all subscribed **Subscribers** whenever a change occurs. 

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When one object changing state requires changing an unknown or dynamic number of other objects. <br>
> Or when an object should notify other objects without making assumptions about what those objects are.**

### Motivation

- How do we keep a varying number of objects, using the same source of data, up to date with changes to that
  data?
- And how do we achieve this without tightly coupling them to the source?

### Known Uses

- Graphical User Interfaces:
    - Respond to a button click or keypress.
    - Update both the graph plot and a pie chart when table data is changed.
    - MVC architecture.
      - Model plays the role of Publisher.
      - While the View is the base class for Subscribers.
- Triggering external actions based on system events.
    - Send a welcome email when a new customer signs up.
    - Backorder an item when a new sale is made causing it to be out of stock.
- Notification Services:
    - Mobile apps provides user with weather updates.
    - Stock price change warnings.
    - Train ticket confirmation when ticket status is changed from waiting to confirmed.
-  Job Queues:
    -  Notify workers when tasks become available in a queue.

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Polymorphism**, **Composition**

Behavioral patterns, in general, are concerned with the assignment of responsibilities between objects.
They’re not just patterns of objects, but also the patterns of communication between those objects.

Behavioral **object** patterns use composition to achieve cooperation between objects.
An important issue here is how these peer objects know about each other.

The observer pattern manages how peers know each other by indirection, to allow for loose coupling.

### Aspects that can vary

- Number of objects that depend on another object;
- How the dependent objects stay up to date.

### Solution to causes of redesign

- Tight coupling.
  - Hard to understand: a lot of context needs to be known to understand a part of the system.
  - Hard to change: changing one class necessitates changing many other classes. 
  - Hard to reuse in isolation: because classes depend on each other.
- Extending functionality by subclassing.
  - Hard to understand: comprehending a subclass requires in-depth knowledge of all parent classes.
  - Hard to change: a modification to a parent might break multiple children classes.
  - Hard to reuse partially: all class members are inherited, even when not applicable, which can lead to a class explosion.

### Consequences

| Advantages                                                                                                                                                                                                                  | Disadvantages                                                                                                                                     |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Loose coupling between Publisher and Subscriber.** <br> All the publisher knows is that it has a list of objects that conform to the subscriber interface. It doesn't know the concrete class of any subscriber. | :x: **No control over notification order** <br> In the typical implementation you can’t control the order in which subscribers are notified       |
| :heavy_check_mark: **Open/Closed Principle** <br> You can introduce new subscriber classes without having to change the publisher code (and vice versa if there’s a publisher interface).                                         | :x: **Performance** <br> A small state change on the publisher can cause a cascade of updates to subscribers and their dependent objects.             |
| :heavy_check_mark: **Dynamic connection between objects** <br> You can establish relations between objects at runtime.                                                                                                      | :x: **Logging / Tracking** <br> Communications through events (especially if they are asynchronous) make it harder to read logs and troubleshoot. |

### Relations with Other Patterns

_Distinction from other patterns:_

- Chain of Responsibility passes a request sequentially along a dynamic chain of potential receivers until one of
  them handles it.
- Command establishes unidirectional connections between senders and receivers.
- Mediator eliminates direct connections between senders and receivers, forcing them to communicate indirectly via a
  mediator object.
- 
<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A Publisher class with a subscribe method to store subscribers.**
> **And a Subscriber class with an update method, which the publisher calls to notify it of changes.**

### Structure

```mermaid
classDiagram
    class Publisher {
        <<interface>>
        + subscribe(subscriber: Subscriber)*
        + unsubscribe(subscriber: Subscriber)*
        + notify()*
    }

    class Subscriber {
        <<interface>>
        + update()*
    }

    class ConcretePublisherA {
        - publisherState
        + getState()
        + setState(state)
    }
    
    class ConcretePublisherB {
        - publisherState
        + getState()
        + setState(state)
    }

    class ConcreteSubscriberA {
        - subscriberState
        + update()
    }

    class ConcreteSubscriberB {
        - subscriberState
        + update()
    }

    ConcretePublisherA ..|> Publisher: implements
    ConcretePublisherB ..|> Publisher: implements
    Subscriber <|.. ConcreteSubscriberA: implements
    Subscriber <|.. ConcreteSubscriberB: implements
    Publisher o-- Subscriber: aggregated by
```

### Variations

_The push and pull models:_

- **Push**: The publisher supplies the data as arguments to the `update` method of the subscriber.
    - :heavy_check_mark: The subscribers don’t need to know anything about the publisher.
    - :x: The publisher needs to know something about the needs of the subscribers.
    - :x: Harder for subscribers to figure out what changed.
- **Pull**: The publisher supplies itself as an argument to the `update` method, allowing the subscriber to use the publishers
  public `get` methods to retrieve information.
    - :heavy_check_mark: The publisher only needs to provide public getters, the subscribers can get the information they
      need themselves
    - :x: Subscribers become coupled to publisher interface.

_Who triggers the update:_

- **Automatic**: Setting state calls `notify` automatically
    - :heavy_check_mark: Client can never forget to call notify.
    - :x: Less control.
        - Notify could be called before all state has been made consistent
        - Or every time state is set, notify is called, which can be inefficient.
- **Manual**: Client responsible for calling `notify` 
    - :heavy_check_mark: More control, can call notify after setting state multiple times.
    - :x: Client can forget.

### Implementation

In the example we apply the observer pattern to a simple point of sale system.
Subscribers are used to notify external systems of a made sale.
It only showcases the use of a _Push_ variant with _Automatic_ updating.

- [Publisher (interface)](../observer/pattern/sale_publisher.py)
- [Concrete Publisher](../observer/pattern/retail_sale_publisher.py)
- [Subscriber (interface)](../observer/pattern/sale_subscriber.py)
- [Concrete Subscriber 1](../observer/pattern/commission_subscriber.py)
- [Concrete Subscriber 2](../observer/pattern/backorder_subscriber.py)

The _client_ (the system in which events happen that we want to publish) is a sale registration program:

- [Client](../observer/clients/sale_pro.py)

The unit tests exercise the public interface of the Publisher and Subscribers:

- [Concrete Publisher test](../tests/unit/test_sale_publisher.py)
- [Concrete Subscriber test](../tests/unit/test_sale_subscribers.py)

<br>
<br>

## Sources

- [Design Patterns: Elements of Reusable Object-Oriented Software](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Observer](https://refactoring.guru/design-patterns/observer)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ArjanCodes: Subscriber Pattern Tutorial: I NEVER Knew Events Were THIS Powerful](https://www.youtube.com/watch?v=oNalXg67XEE)
- [Geekific: The Observer Pattern Explained and Implemented in Java](https://youtu.be/-oLDJ2dbadA?si=Hp6BR4L0_q2ZGZZ8)
