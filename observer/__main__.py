from observer.clients.sale_pro import SalePro
from observer.lib.catalogue import Catalogue
from observer.lib.prompts import (
    display_backorder,
    display_commission,
    display_revenue,
    select_sold_products_prompt,
    select_subscribers_prompt,
)
from observer.lib.transaction import Dollar, Percentage, Product, Vendor
from observer.pattern.backorder_subscriber import BackorderSubscriber
from observer.pattern.commission_subscriber import CommissionSubscriber
from observer.pattern.retail_sale_publisher import RetailSalePublisher


def setup_sale_pro() -> SalePro:
    sale_publisher = RetailSalePublisher(revenue_writer=display_revenue)

    product_catalogue = Catalogue(
        items=[
            Product("paperclips", price=Dollar(5)),
            Product("staples", price=Dollar(2)),
            Product("paper", price=Dollar(12)),
        ],
    )

    commission_subscriber = CommissionSubscriber(
        name="commission",
        percentage=Percentage(3),
        payment_processor=display_commission,
    )
    backorder_subscriber = BackorderSubscriber(
        name="backorder",
        vendors=[
            Vendor("Dunder Mifflin", minimum_quantity=1),
            Vendor("Office Depot", minimum_quantity=3),
        ],
        order_processor=display_backorder,
    )
    subscriber_catalogue = Catalogue(items=[commission_subscriber, backorder_subscriber])

    return SalePro(
        sale_publisher=sale_publisher,
        product_catalogue=product_catalogue,
        subscriber_catalogue=subscriber_catalogue,
    )


if __name__ == "__main__":
    sale_pro = setup_sale_pro()
    sale_pro.run(
        subscriber_selector=select_subscribers_prompt,
        sold_products_selector=select_sold_products_prompt,
    )
