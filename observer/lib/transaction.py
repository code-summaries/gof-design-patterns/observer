from __future__ import annotations

from dataclasses import dataclass
from typing import Union


@dataclass(frozen=True)
class Dollar:
    amount: float

    def __radd__(self, other: Union[Dollar, float, int]) -> Dollar:
        return self._add(other)

    def __add__(self, other: Union[Dollar, float, int]) -> Dollar:
        return self._add(other)

    def __rmul__(self, other: float) -> Dollar:
        return Dollar(self.amount * other)

    def __str__(self) -> str:
        return f"${self.amount:.2f}"

    def _add(self, other: Union[Dollar, float, int]) -> Dollar:
        if isinstance(other, Dollar):
            return Dollar(self.amount + other.amount)
        if isinstance(other, (int, float)):
            return Dollar(self.amount + other)
        raise TypeError(f"unsupported operand type(s) for +: '{Dollar}' and '{type(other)}'")


@dataclass(frozen=True)
class Product:
    name: str
    price: Dollar

    def get_name(self) -> str:
        return self.name


@dataclass(frozen=True)
class Vendor:
    name: str
    minimum_quantity: int


@dataclass(frozen=True)
class Percentage:
    amount: int

    def __mul__(self, other: Dollar) -> Dollar:
        return (self.amount / 100.0) * other


@dataclass
class SaleEvent:
    total_price: Dollar
    quantity: int
