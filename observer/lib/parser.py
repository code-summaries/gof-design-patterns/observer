def parse_prompt_input(selection_text: str) -> list[str]:
    names = [s.strip() for s in selection_text.split(",")]
    return [n for n in names if n]
