from __future__ import annotations

from typing import Generic, Protocol, TypeVar


class WithGetName(Protocol):
    def get_name(self) -> str:
        ...


T = TypeVar("T", bound=WithGetName)


class Catalogue(Generic[T]):
    def __init__(self, items: list[T]) -> None:
        self._item_mapping = {item.get_name(): item for item in items}

    def _get_single_item(self, item_name: str) -> T | None:
        return self._item_mapping.get(item_name)

    def get_items(self, item_names: list[str]) -> list[T]:
        items = [self._get_single_item(n) for n in item_names]
        return [i for i in items if i is not None]

    def get_available_item_names(self) -> list[str]:
        return list(self._item_mapping.keys())
