from observer.lib.catalogue import Catalogue
from observer.lib.parser import parse_prompt_input
from observer.lib.transaction import Dollar, Product, Vendor
from observer.pattern.sale_subscriber import SaleSubscriber


def select_subscribers_prompt(subscriber_catalogue: Catalogue[SaleSubscriber]) -> list[str]:
    options = "\n    ".join(subscriber_catalogue.get_available_item_names())
    prompt_text = (
        "\nWelcome to SalePro, the system that lets you register your sales! \n"
        "Please write the names of services you want to use, "
        "separated by commas, and press enter. \n"
        "Or leave it blank to not use any services: \n"
        f"    {options}\n"
    )
    return parse_prompt_input(input(prompt_text))


def select_sold_products_prompt(product_catalogue: Catalogue[Product]) -> list[str]:
    options = "\n    ".join(product_catalogue.get_available_item_names())
    prompt_text = (
        "Please write the names of the sales that were made, "
        "separated by commas, and press enter: \n"
        f"    {options}\n"
    )
    return parse_prompt_input(input(prompt_text))


def display_revenue(revenue: Dollar) -> None:
    print(f"\nWrote revenue of: {revenue}.")


def display_commission(commission: Dollar) -> None:
    print(f"Paid a commission of: {commission}.")


def display_backorder(vendor: Vendor) -> None:
    print(f"Backorder placed at: {vendor.name}.")
