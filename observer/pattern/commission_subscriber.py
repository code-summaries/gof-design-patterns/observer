from __future__ import annotations

from typing import Callable

from observer.lib.transaction import Dollar, Percentage, SaleEvent
from observer.pattern.sale_subscriber import SaleSubscriber


class CommissionSubscriber(SaleSubscriber):
    def __init__(
        self, name: str, percentage: Percentage, payment_processor: Callable[[Dollar], None]
    ) -> None:
        self._name = name
        self._percentage = percentage
        self._process_payment = payment_processor

    def get_name(self) -> str:
        return self._name

    def update(self, sale_event: SaleEvent) -> None:
        commission = self._percentage * sale_event.total_price
        self._process_payment(commission)
