from abc import ABC, abstractmethod

from observer.lib.transaction import Product
from observer.pattern.sale_subscriber import SaleSubscriber


class SalePublisher(ABC):
    @abstractmethod
    def subscribe(self, subscriber: SaleSubscriber) -> None:
        raise NotImplementedError

    @abstractmethod
    def register_sale(self, products: list[Product]) -> None:
        raise NotImplementedError
