from __future__ import annotations

from abc import ABC, abstractmethod

from observer.lib.transaction import SaleEvent


class SaleSubscriber(ABC):
    @abstractmethod
    def get_name(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def update(self, sale_event: SaleEvent) -> None:
        raise NotImplementedError
