from typing import Callable

from observer.lib.transaction import Dollar, Product, SaleEvent
from observer.pattern.sale_publisher import SalePublisher
from observer.pattern.sale_subscriber import SaleSubscriber


class RetailSalePublisher(SalePublisher):
    def __init__(self, revenue_writer: Callable[[Dollar], None]):
        self._write_revenue = revenue_writer
        self._subscribers: list[SaleSubscriber] = []

    def subscribe(self, subscriber: SaleSubscriber) -> None:
        self._subscribers.append(subscriber)

    def register_sale(self, products: list[Product]) -> None:
        total_price = self._calculate_total_price(products)
        quantity = self._calculate_quantity(products)

        self._write_revenue(total_price)
        self._notify(SaleEvent(total_price=total_price, quantity=quantity))

    @staticmethod
    def _calculate_total_price(products: list[Product]) -> Dollar:
        return sum(p.price for p in products)  # type: ignore

    @staticmethod
    def _calculate_quantity(products: list[Product]) -> int:
        return len(products)

    def _notify(self, sale_event: SaleEvent) -> None:
        for subscriber in self._subscribers:
            subscriber.update(sale_event)
