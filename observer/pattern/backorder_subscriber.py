from __future__ import annotations

from typing import Callable

from observer.lib.transaction import SaleEvent, Vendor
from observer.pattern.sale_subscriber import SaleSubscriber


class BackorderSubscriber(SaleSubscriber):
    def __init__(
        self, name: str, vendors: list[Vendor], order_processor: Callable[[Vendor], None]
    ) -> None:
        self._name = name
        self._vendors = self._sort_vendors(vendors)
        self._process_order = order_processor

    def get_name(self) -> str:
        return self._name

    def update(self, sale_event: SaleEvent) -> None:
        vendor = self._find_vendor(sale_event.quantity)
        if vendor:
            self._process_order(vendor)

    def _find_vendor(self, quantity: int) -> Vendor | None:
        for vendor in self._vendors:
            if quantity >= vendor.minimum_quantity:
                return vendor
        return None

    @staticmethod
    def _sort_vendors(vendors: list[Vendor]) -> list[Vendor]:
        return sorted(vendors, key=lambda v: v.minimum_quantity, reverse=True)
