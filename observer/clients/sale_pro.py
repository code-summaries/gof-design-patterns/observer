from typing import Callable, TypeAlias

from observer.lib.catalogue import Catalogue
from observer.lib.transaction import Product
from observer.pattern.sale_publisher import SalePublisher
from observer.pattern.sale_subscriber import SaleSubscriber

SubSelect: TypeAlias = Callable[[Catalogue[SaleSubscriber]], list[str]]
ProdSelect: TypeAlias = Callable[[Catalogue[Product]], list[str]]


class SalePro:
    def __init__(
        self,
        sale_publisher: SalePublisher,
        product_catalogue: Catalogue[Product],
        subscriber_catalogue: Catalogue[SaleSubscriber],
    ) -> None:
        self._sale_publisher = sale_publisher
        self._product_catalogue = product_catalogue
        self._subscriber_catalogue = subscriber_catalogue

    def run(self, subscriber_selector: SubSelect, sold_products_selector: ProdSelect) -> None:
        self._add_subscribers(subscriber_selector)
        self._register_sale(sold_products_selector)

    def _add_subscribers(self, subscriber_selector: SubSelect) -> None:
        subscriber_names = subscriber_selector(self._subscriber_catalogue)
        subscribers = self._subscriber_catalogue.get_items(subscriber_names)

        for subscriber in subscribers:
            self._sale_publisher.subscribe(subscriber)

    def _register_sale(self, sold_products_selector: ProdSelect) -> None:
        sold_product_names = sold_products_selector(self._product_catalogue)
        sold_products = self._product_catalogue.get_items(sold_product_names)

        self._sale_publisher.register_sale(sold_products)
