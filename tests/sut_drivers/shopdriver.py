from typing import Any

from observer.clients.sale_pro import SalePro
from observer.lib.catalogue import Catalogue
from observer.lib.transaction import Dollar, Percentage, Product, Vendor
from observer.pattern.backorder_subscriber import BackorderSubscriber
from observer.pattern.commission_subscriber import CommissionSubscriber
from observer.pattern.retail_sale_publisher import RetailSalePublisher
from tests.test_doubles.spies import BackorderSpy, PaymentSpy


class ShopDriver:
    def __init__(self) -> None:
        self.selected_subscriber_names: list[str] = []

        self.revenue_spy = PaymentSpy()
        self.commission_spy = PaymentSpy()
        self.backorder_spy = BackorderSpy()

        sale_publisher = RetailSalePublisher(revenue_writer=self.revenue_spy.set_amount)
        product_catalogue = Catalogue(
            items=[
                Product("paperclips", price=Dollar(5)),
                Product("staples", price=Dollar(2)),
            ],
        )

        commission_subscriber = CommissionSubscriber(
            name="commission",
            percentage=Percentage(3),
            payment_processor=self.commission_spy.set_amount,
        )
        backorder_subscriber = BackorderSubscriber(
            name="backorder",
            vendors=[Vendor("Dunder Mifflin", minimum_quantity=1)],
            order_processor=self.backorder_spy.place_order,
        )
        subscriber_catalogue = Catalogue(items=[commission_subscriber, backorder_subscriber])

        self.sale_pro = SalePro(
            sale_publisher=sale_publisher,
            product_catalogue=product_catalogue,
            subscriber_catalogue=subscriber_catalogue,
        )

    def select_subscribers(self, subscriber_names: list[str]) -> None:
        self.selected_subscriber_names = subscriber_names

    def register_sale(self, selected_product_names: list[str]) -> None:
        self.sale_pro.run(
            subscriber_selector=lambda x: self.selected_subscriber_names,
            sold_products_selector=lambda x: selected_product_names,
        )

    def assert_revenue_written(self, expected_revenue: str) -> None:
        actual_revenue = str(self.revenue_spy.get_amount())
        self._assert_equals(expected_revenue, actual_revenue)

    def assert_commission_paid(self, expected_commission: str) -> None:
        actual_commission = str(self.commission_spy.get_amount())
        self._assert_equals(expected_commission, actual_commission)

    def assert_backorder_placed_at_vendor(self, expected_vendor: str) -> None:
        actual_vendor = self.backorder_spy.get_vendor_of_order().name
        self._assert_equals(expected_vendor, actual_vendor)

    @staticmethod
    def _assert_equals(expected: Any, actual: Any) -> None:
        assert expected == actual, f"Expected: '{expected}', got: '{actual}'."
