import unittest

from tests.sut_drivers.shopdriver import ShopDriver


class TestSalesPro(unittest.TestCase):
    def setUp(self) -> None:
        self.shop = ShopDriver()

    def test_register_sale_writes_revenue(self) -> None:
        self.shop.select_subscribers([])

        self.shop.register_sale(["paperclips", "staples"])

        self.shop.assert_revenue_written("$7.00")

    def test_commission_subscriber_pays_calculated_amount(self) -> None:
        self.shop.select_subscribers(["commission"])

        self.shop.register_sale(["paperclips"])

        self.shop.assert_commission_paid("$0.15")

    def test_backorder_subscriber_orders_from_vendor(self) -> None:
        self.shop.select_subscribers(["backorder"])

        self.shop.register_sale(["paperclips"])

        self.shop.assert_backorder_placed_at_vendor("Dunder Mifflin")
