import unittest

from observer.lib.transaction import Dollar, Product, SaleEvent
from observer.pattern.retail_sale_publisher import RetailSalePublisher
from tests.test_doubles.spies import PaymentSpy, SubscriberSpy


class TestSalePublisher(unittest.TestCase):
    def setUp(self) -> None:
        self.sale_spy = PaymentSpy()
        self.sale_publisher = RetailSalePublisher(revenue_writer=self.sale_spy.set_amount)
        self.spy_subscriber = SubscriberSpy()
        self.spy_subscriber_b = SubscriberSpy()

    def test_single_products_sold(self) -> None:
        self.sale_publisher.register_sale([Product("paperclips", price=Dollar(5))])
        self.assertEqual(Dollar(5), self.sale_spy.get_amount())

    def test_multiple_products_sold(self) -> None:
        self.sale_publisher.register_sale(
            [
                Product("paperclips", price=Dollar(5)),
                Product("staples", price=Dollar(2)),
            ]
        )
        self.assertEqual(Dollar(7), self.sale_spy.get_amount())

    def test_single_subscriber_notified(self) -> None:
        self.sale_publisher.subscribe(self.spy_subscriber)
        self.sale_publisher.register_sale([Product("paperclips", price=Dollar(5))])
        self.assertEqual(
            SaleEvent(total_price=Dollar(5), quantity=1), self.spy_subscriber.received_event()
        )

    def test_multiple_subscribers_notified(self) -> None:
        self.sale_publisher.subscribe(self.spy_subscriber)
        self.sale_publisher.subscribe(self.spy_subscriber_b)
        self.sale_publisher.register_sale([Product("paperclips", price=Dollar(5))])

        expected_events = [
            SaleEvent(total_price=Dollar(5), quantity=1),
            SaleEvent(total_price=Dollar(5), quantity=1),
        ]
        received_events = [
            self.spy_subscriber.received_event(),
            self.spy_subscriber_b.received_event(),
        ]
        self.assertEqual(expected_events, received_events)
