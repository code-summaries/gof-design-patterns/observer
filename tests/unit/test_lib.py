import unittest

from observer.lib.catalogue import Catalogue
from observer.lib.parser import parse_prompt_input
from observer.lib.transaction import Dollar, Product


class TestDollar(unittest.TestCase):
    def test_sum_one_dollar_returns_one_dollar(self) -> None:
        self.assertEqual(Dollar(1), sum([Dollar(1)]))

    def test_sum_one_and_two_dollars_returns_three_dollars(self) -> None:
        self.assertEqual(Dollar(3), sum([Dollar(1), Dollar(2)]))


class TestCatalogue(unittest.TestCase):
    def setUp(self) -> None:
        self.catalogue = Catalogue(
            items=[
                Product("paperclips", price=Dollar(5)),
                Product("staples", price=Dollar(2)),
            ]
        )

    def test_get_one_item_by_name_returns_item(self) -> None:
        self.assertEqual(
            [Product("paperclips", price=Dollar(5))],
            self.catalogue.get_items(["paperclips"]),
        )

    def test_two_items_by_name_returns_items(self) -> None:
        self.assertEqual(
            [
                Product("paperclips", price=Dollar(5)),
                Product("staples", price=Dollar(2)),
            ],
            self.catalogue.get_items(["paperclips", "staples"]),
        )

    def test_get_item_with_invalid_name_is_ignored(self) -> None:
        self.assertEqual([], self.catalogue.get_items(["perpetuum mobile"]))

    def test_get_available_item_names_returns_list_of_item_names(self) -> None:
        self.assertEqual(["paperclips", "staples"], self.catalogue.get_available_item_names())


class TestParsePromptInput(unittest.TestCase):
    def test_single_input_name(self) -> None:
        self.assertEqual(["single"], parse_prompt_input("single"))

    def test_single_input_name_with_trailing_comma(self) -> None:
        self.assertEqual(["single"], parse_prompt_input("single, "))

    def test_multiple_input_names(self) -> None:
        self.assertEqual(["one", "two"], parse_prompt_input("one, two"))
