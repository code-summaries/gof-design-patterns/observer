import unittest

from observer.lib.transaction import Dollar, Percentage, SaleEvent, Vendor
from observer.pattern.backorder_subscriber import BackorderSubscriber
from observer.pattern.commission_subscriber import CommissionSubscriber
from tests.test_doubles.spies import BackorderSpy, PaymentSpy


class TestCommissionSubscriber(unittest.TestCase):
    def setUp(self) -> None:
        self.payment_spy = PaymentSpy()

    def test_pay_commission(self) -> None:
        commission_subscriber = CommissionSubscriber(
            name="commission",
            percentage=Percentage(3),
            payment_processor=self.payment_spy.set_amount,
        )
        commission_subscriber.update(SaleEvent(total_price=Dollar(5), quantity=1))
        self.assertEqual(Dollar(0.15), self.payment_spy.get_amount())


class TestBackorderSubscriber(unittest.TestCase):
    def setUp(self) -> None:
        self.backorder_spy = BackorderSpy()
        self.dunder_mifflin = Vendor("Dunder Mifflin", minimum_quantity=1)
        self.office_depot = Vendor("Office Depot", minimum_quantity=3)
        self.backorder_subscriber = BackorderSubscriber(
            name="backorder",
            vendors=[self.dunder_mifflin, self.office_depot],
            order_processor=self.backorder_spy.place_order,
        )

    def test_backorder_single_product(self) -> None:
        self.backorder_subscriber.update(SaleEvent(total_price=Dollar(5), quantity=1))
        self.assertEqual(self.dunder_mifflin, self.backorder_spy.get_vendor_of_order())

    def test_backorder_multiple_products(self) -> None:
        self.backorder_subscriber.update(SaleEvent(total_price=Dollar(9), quantity=3))
        self.assertEqual(self.office_depot, self.backorder_spy.get_vendor_of_order())
