from observer.lib.transaction import Dollar, SaleEvent, Vendor
from observer.pattern.sale_subscriber import SaleSubscriber


class ReporterSpy:
    def __init__(self) -> None:
        self._report = ""

    def set_report(self, report: str) -> None:
        self._report = report

    def get_report(self) -> str:
        return self._report


class PaymentSpy:
    def __init__(self) -> None:
        self._amount = Dollar(0)

    def set_amount(self, amount: Dollar) -> None:
        self._amount = amount

    def get_amount(self) -> Dollar:
        return self._amount


class BackorderSpy:
    def __init__(self) -> None:
        self._vendor = Vendor(name="", minimum_quantity=-1)

    def place_order(self, vendor: Vendor) -> None:
        self._vendor = vendor

    def get_vendor_of_order(self) -> Vendor:
        return self._vendor


class SubscriberSpy(SaleSubscriber):
    def __init__(self) -> None:
        self._sale_event = SaleEvent(total_price=Dollar(0), quantity=0)

    def update(self, sale_event: SaleEvent) -> None:
        self._sale_event = sale_event

    def received_event(self) -> SaleEvent:
        return self._sale_event

    def get_name(self) -> str:
        return "subscriber_spy"
